import React from "react";

export default class Velocidade extends React.Component{

    alterarValor = (evento) =>{

        let valor = evento.target.value
        this.props.alterar(valor)

    }
    render = () =>{
        
        return <div>
            <input type = 'number' min = '0' max ='3' onChange ={this.alterarValor} value = {this.props.valor}></input>
        </div>    
    }
}