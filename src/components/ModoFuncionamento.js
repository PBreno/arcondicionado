import React from "react";

export default class ModoFuncionamento extends React.Component{

    botaoPresionado = (evento) =>{
    
        let valor = evento.target.id
        this.props.alterar(valor)

    }
    render = () =>{

        return <div>

            <button onClick = {this.botaoPresionado} id = '0'>VENTILAR</button>
            <button onClick = {this.botaoPresionado} id = '1'>REFRIGERAR</button>
            <button onClick = {this.botaoPresionado} id = '2'>AQUECER</button>
            
        </div>
    }
}